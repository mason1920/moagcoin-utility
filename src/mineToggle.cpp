#include <moagcoin-utility/mineToggle.h>

namespace moag {
	mineToggle::mineToggle(bool *mine) {
		this->mine = mine;
		set_image_from_icon_name("media-playback-start-symbolic");
		signal_toggled().connect(
			sigc::mem_fun(*this, &mineToggle::toggle)
		);
	}

	void mineToggle::toggle() {
		if (*mine) {
			*mine = false;
		} else {
			*mine = true;
		}
	}
}
