#include <moagcoin-utility/wallet.h>

// CppCodec:
#include <hex_upper.hpp>
#include <base64_rfc4648.hpp>

#include <sys/stat.h>

#include <fstream>

// OpenSSL:
#include <openssl/ec.h>
#include <openssl/obj_mac.h>
#include <openssl/sha.h>

namespace moag {
	wallet::wallet(std::string walletDir) {
		// Might not work on Windows as is.
		mkdir(walletDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

		std::ifstream ifstream;
		ifstream.open(walletDir + "wallet");

		std::string publicAddress;
		ifstream >> publicAddress;

		if (publicAddress == "") {
			EC_KEY *key{EC_KEY_new_by_curve_name(NID_secp256k1)};
			EC_GROUP *group{EC_GROUP_new_by_curve_name(NID_secp256k1)};

			EC_KEY_set_group(key, group);

			EC_KEY_generate_key(key);

			const EC_POINT *public_point{EC_KEY_get0_public_key(key)};
			const BIGNUM *private_bignum{EC_KEY_get0_private_key(key)};

			point_conversion_form_t form{
				EC_GROUP_get_point_conversion_form(group)
			};

			BN_CTX *ctx{BN_CTX_new()};

			unsigned char buf[100];
			size_t buf_size{
				EC_POINT_point2oct(group, public_point, form, buf, 100, ctx)
			};

			public_key = cppcodec::base64_rfc4648::encode(buf, buf_size);
			private_key = BN_bn2hex(private_bignum);

			std::ofstream ofstream;
			ofstream.open(walletDir + "wallet");

			ofstream << public_key << '\n' << private_key << '\n';

			ofstream.close();
		}

		ifstream.close();
	}
}
