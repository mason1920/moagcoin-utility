#include <moagcoin-utility/threadsCombo.h>

#include <thread>
#include <string>

namespace moag {
	threadsCombo::threadsCombo(unsigned int *threads) {
		threadCount = std::thread::hardware_concurrency();
		this->threads = threads;
		for (size_t thread = 1; thread <= threadCount; thread++) {
			append(std::to_string(thread) + " Thread");
		}
		set_active(0);
		signal_changed().connect(
			sigc::mem_fun(*this, &threadsCombo::updateThreads)
		);
	}

	void threadsCombo::updateThreads() {
		*threads = get_active_row_number() + 1;
	}
}
