// C++ Standard Library:
#include <string>
#include <iostream>
#include <thread>
#include <iomanip>

// C POSIX Library:
#include <sys/mman.h> // Shared memory for IPC.

// Linuxisms:
#include <sys/prctl.h> // Process management for kernel signals.

// GTKmm:
#include <gtkmm/application.h>
#include <gtkmm/window.h>
#include <gtkmm/grid.h>

// OpenSSL:
#include <openssl/sha.h>

// CURLpp:
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

// JsonCpp:
#include <json/json.h>

// Moagcoin Utility:
#include <moagcoin-utility.h>

// Closes mining loop if window closes without toggling off mining first.
void sigquit(int foo) {
	exit(0);
}

// Gets SHA256 hash of a string.
std::string sha256(const std::string str) {
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, str.c_str(), str.size());
    SHA256_Final(hash, &sha256);
    std::stringstream ss;
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
    }
    return ss.str();
}

// Brute force mining loop.
// Threads get brought here when mining is enabled.
void prove(Json::Value block, bool &found, std::string publicAddress, bool *mine) {
	std::string challenge{block["hash"].asString() + "80"};
	std::string answer;
	std::string choices{
		"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz0123456789"
	};
	std::string attempt;
	std::string solution;
	while (!found && *mine) {
		answer = "";
		for (size_t letter = 0; letter < 80; letter++) {
			answer += choices[rand() % 62];
		}
		attempt = challenge + answer;
		solution = sha256(attempt);
		if (solution.find("000000") == 0) {
			found = true;
			block["attempt"] = attempt;
			block["miner"] = publicAddress;
			std::stringstream stream;
			std::string postFields{
				"from='" + block["from"].asString()
				+"'&to='" + block["to"].asString()
				+"'&amount='" + block["amount"].asString()
				+"'&signature='" + block["signature"].asString()
				+"'&message='" + block["message"].asString()
				+"'&timestamp=" + block["timestamp"].asString()
				+"&nonce=" + block["nonce"].asString()
				+"&header='" + block["header"].asString()
				+"'&validated='" + block["validated"].asString()
				+"'&hash='" + block["hash"].asString()
				+"'&attempt='" + block["attempt"].asString()
				+"'&miner='" + block["miner"].asString()
				+"'"
			};
			curlpp::Cleanup cleanup;
			curlpp::Easy request;
			request.setOpt<curlpp::options::Url>(
				"http://35.209.72.253:443/confirmBlock"
			);
			request.setOpt<curlpp::options::WriteStream>(&stream);
			request.setOpt<curlpp::options::PostFields>(postFields);
			request.setOpt<curlpp::options::PostFieldSize>(postFields.length());
			request.perform();
		}
	}
}

int main() {
	// Shared variables for IPC between GUI and Miner.

	// The mining button toggles its value.
	// The mining loop only runs if true.
	bool *mine{
		(bool*)
		mmap(NULL, 1, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0)
	};

	// The parent's thread combo box sets the value to the selected thread count.
	// The child makes however many threads were selected.
	unsigned int *threads{
		(unsigned int*)
		mmap(NULL, 1, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0)
	};

	*threads = 1;

	// Not sure how portable this is.
	// Definitely won't work on Windows.
	std::string localShare{getenv("HOME")};
	localShare += "/.local/share/moagcoin-utility/";

	moag::wallet wallet(localShare);

	// Won't work on Windows.
	pid_t pid{fork()};

	// If parent set up GUI.
	// Else child, ready the mining loop.
	if (pid) {
		auto app{Gtk::Application::create()};

		Gtk::Window window;
		window.set_title("Moagcoin Utility");

		Gtk::Grid dashboard;
		dashboard.set_halign(Gtk::ALIGN_CENTER);
		dashboard.set_valign(Gtk::ALIGN_CENTER);
		dashboard.set_margin_top(4);
		dashboard.set_margin_right(4);
		dashboard.set_margin_bottom(4);
		dashboard.set_margin_left(4);
		window.add(dashboard);

		moag::balanceDisplay balanceDisplay(wallet.public_key);
		dashboard.attach(balanceDisplay, 0, 0);

		Gtk::Grid mineControl;
		mineControl.set_margin_top(1);
		mineControl.set_margin_right(1);
		mineControl.set_margin_bottom(1);
		mineControl.set_margin_left(1);
		mineControl.get_style_context()->add_class("linked");
		dashboard.attach(mineControl, 0, 1);

		moag::mineToggle mineToggle(mine);
		mineControl.attach(mineToggle, 0, 0);

		moag::threadsCombo threadsCombo(threads);
		mineControl.attach(threadsCombo, 1, 0);

		window.show_all();

		return app->run(window);
	} else {
		// No Windows or Mac.
		nice(5); // POSIX
		signal(SIGQUIT, sigquit); // POSIX
		prctl(PR_SET_PDEATHSIG, SIGQUIT); // Linux

		// Gets a block and makes *threads amount of threads to go mine.
		// Once one thread finishes mining, they all stop.
		// This repeats as long as the user wants to mine.
		while (true) {
			if (*mine) {
				std::stringstream stream;
				curlpp::Cleanup cleanup;
				curlpp::Easy request;
				request.setOpt<curlpp::options::Url>(
					"http://35.209.72.253:443/unclaimedBlock"
				);
				request.setOpt<curlpp::options::WriteStream>(&stream);
				request.perform();
				Json::Value block;
				std::string tmp{stream.str()};
				std::replace(tmp.begin(), tmp.end(), '\'', '\"');
				stream.str(tmp);
				stream >> block;
				bool found{false};
				auto currentThreads{*threads};
				std::thread *army[currentThreads];
				for (size_t thread = 0; thread < currentThreads; thread++) {
					army[thread] = new std::thread(prove, block, std::ref(found), wallet.public_key, mine);
				}
				for (size_t thread = 0; thread < currentThreads; thread++) {
					army[thread]->join();
				}
			} else {
				sleep(1);
			}
		}
	}
}
