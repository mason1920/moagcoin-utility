#include <moagcoin-utility/balanceDisplay.h>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

namespace moag {
	balanceDisplay::balanceDisplay(std::string publicAddress) {
		this->publicAddress = publicAddress;
		set_margin_top(1);
		set_margin_right(1);
		set_margin_bottom(1);
		set_margin_left(1);
		set_image_from_icon_name("view-refresh-symbolic");
		set_always_show_image();
		signal_clicked().connect(
			sigc::mem_fun(*this, &balanceDisplay::update)
		);
		update();
	}

	void balanceDisplay::update() {
		std::ostringstream stream;
		std::string postFields{"address=" + publicAddress};
		curlpp::Cleanup cleanup;
		curlpp::Easy request;
		request.setOpt<curlpp::options::Url>("http://35.209.72.253:443/balance");
		request.setOpt<curlpp::options::WriteStream>(&stream);
		request.setOpt<curlpp::options::PostFields>(postFields);
		request.setOpt<curlpp::options::PostFieldSize>(postFields.length());
		request.perform();
		set_label(stream.str());
	}
}
