# Moagcoin Utility

moagCoin is a centralized cryptocurrency made by [benjimaestro.](https://github.com/benjimaestro/)

My utility is mostly a translation of [Benji's python miner](https://github.com/benjimaestro/moagcoin) into C++.

It's not complete, but it does have a possibly functioning miner which may or may not have better performance than the leading competitor.

Right now, it's unstable and not recommended for mining unattended (mostly because there's no system in place to queue completed blocks when the server goes down and probably crashes the program).

## Linux Installation

### From Binaries

1.  Install the dependencies:
*  [libc6](https://www.gnu.org/software/libc/libc.html)
*  [libgtkmm-3.0-1v5](https://www.gtkmm.org/)
*  [openssl](https://www.openssl.org/)
*  [libcurlpp0](https://www.curlpp.org/)
*  [libjsoncpp1](https://github.com/open-source-parsers/jsoncpp)
2.  Download the [latest release.](https://gitlab.com/mason1920/moagcoin-utility/-/releases)
3.  Open a terminal in the project directory.
4.  `make install`

### From Source

1.  Install the dependencies:
*  [libc6-dev](https://www.gnu.org/software/libc/libc.html)
*  [libgtkmm-3.0-dev](https://www.gtkmm.org/)
*  [openssl](https://www.openssl.org/)
*  [libcurlpp-dev](https://www.curlpp.org/)
*  [libjsoncpp-dev](https://github.com/open-source-parsers/jsoncpp)
2.  Download the [master branch](https://gitlab.com/mason1920/moagcoin-utility/-/archive/master/moagcoin-utility-master.tar.bz2) or the [latest release's](https://gitlab.com/mason1920/moagcoin-utility/-/releases) source.
3.  Open a terminal in the project directory.
4.  `make`
5.  `make install`
