#ifndef THREADSCOMBO_H
#define THREADSCOMBO_H

#include <gtkmm/comboboxtext.h>

namespace moag {
	class threadsCombo : public Gtk::ComboBoxText {
	public:
		threadsCombo(unsigned int *threads);
	private:
		unsigned int threadCount;
		unsigned int *threads;
		void updateThreads();
	};
}

#endif
