#ifndef MINETOGGLE_H
#define MINETOGGLE_H

#include <gtkmm/togglebutton.h>

namespace moag {
	class mineToggle : public Gtk::ToggleButton {
	public:
		mineToggle(bool *mine);
	private:
		bool *mine;
		void toggle();
	};
}

#endif
