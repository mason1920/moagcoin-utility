#ifndef BALANCEDISPLAY_H
#define BALANCEDISPLAY_H

#include <gtkmm/button.h>
#include <string>

namespace moag {
	class balanceDisplay : public Gtk::Button {
	public:
		balanceDisplay(std::string publicAddress);
	private:
		std::string publicAddress;
		void update();
	};
}

#endif
