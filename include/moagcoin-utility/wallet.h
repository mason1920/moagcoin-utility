#ifndef WALLET_H
#define WALLET_H

#include <string>

namespace moag {
	class wallet {
	public:
		wallet(std::string walletDir);
		std::string public_key;
		std::string private_key;
	};
}

#endif
