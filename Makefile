SHELL := /bin/dash

GTKMM_FLAGS = $(shell pkg-config --cflags --libs gtkmm-3.0)
OPENSSL_FLAGS = $(shell pkg-config --cflags --libs openssl)
CURLPP_FLAGS = $(shell pkg-config --cflags --libs curlpp)
JSONCPP_FLAGS = $(shell pkg-config --cflags --libs jsoncpp)

GIT_TAG = $(shell git describe --tags $(shell git rev-list --tags --max-count=1))

KERNEL = $(shell uname -s)
ARCH = $(shell uname -m)

.SILENT:

all: mkdir obj/main.o obj/balanceDisplay.o obj/mineToggle.o obj/threadsCombo.o obj/wallet.o
	g++ -o bin/moagcoin-utility obj/main.o obj/balanceDisplay.o obj/mineToggle.o obj/threadsCombo.o obj/wallet.o $(OPENSSL_FLAGS) $(GTKMM_FLAGS) $(CURLPP_FLAGS) $(JSONCPP_FLAGS)
	echo "+ bin/moagcoin-utility"

obj/main.o: src/main.cpp
	g++ -c src/main.cpp -o obj/main.o -I include/ $(GTKMM_FLAGS) $(JSONCPP_FLAGS)
	echo "+ obj/main.o"

obj/balanceDisplay.o: src/balanceDisplay.cpp include/moagcoin-utility/balanceDisplay.h
	g++ -c src/balanceDisplay.cpp -o obj/balanceDisplay.o -I include/ $(GTKMM_FLAGS)
	echo "+ obj/balanceDisplay.o"

obj/mineToggle.o: src/mineToggle.cpp include/moagcoin-utility/mineToggle.h
	g++ -c src/mineToggle.cpp -o obj/mineToggle.o -I include/ $(GTKMM_FLAGS)
	echo "+ obj/mineToggle.o"

obj/threadsCombo.o: src/threadsCombo.cpp include/moagcoin-utility/threadsCombo.h
	g++ -c src/threadsCombo.cpp -o obj/threadsCombo.o -I include/ $(GTKMM_FLAGS)
	echo "+ obj/threadsCombo.o"

obj/wallet.o: src/wallet.cpp include/moagcoin-utility/wallet.h
	g++ -c src/wallet.cpp -o obj/wallet.o -I cppcodec/ -I include/ $(GTKMM_FLAGS)
	echo "+ obj/wallet.o"

package:
	rm -rf "moagcoin-utility "*
	tar -cf "moagcoin-utility "$(GIT_TAG)" "$(KERNEL)" "$(ARCH)".tar" bin/moagcoin-utility Makefile
	lrzip -qz -L 9 "moagcoin-utility "$(GIT_TAG)" "$(KERNEL)" "$(ARCH)".tar"
	rm -rf "moagcoin-utility "$(GIT_TAG)" "$(KERNEL)" "$(ARCH)".tar"
	echo "+ ""moagcoin-utility "$(GIT_TAG)" "$(KERNEL)" "$(ARCH)".tar.lrz"

install:
	pkexec rsync $(PWD)/bin/moagcoin-utility /usr/bin/
	echo "* bin/moagcoin-utility -> /usr/bin/moagcoin-utility"

uninstall:
	pkexec rm -rf /usr/bin/moagcoin-utility
	echo "- /usr/bin/moagcoin-utility"

mkdir:
	mkdir -p bin/ obj/

clean:
	rm -rf bin/ obj/ "moagcoin-utility "*".tar.lrz"
	echo "- bin/\n- obj/\n- moagcoin-utility "$(GIT_TAG)" "$(KERNEL)" "$(ARCH)".tar.lrz"
